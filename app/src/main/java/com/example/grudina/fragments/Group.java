package com.example.grudina.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.grudina.structures.Group_structure;
import com.example.grudina.ListItemsAdapter;
import com.example.grudina.R;
import com.example.grudina.activity.User_group;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class Group extends Fragment {

    private ListItemsAdapter arrayAdapter;
    private ArrayList<String> list_of_groups = new ArrayList<>();
    private View.OnClickListener listener;
    private DatabaseReference userGroup;

    public Group() {
    }

    private String userName;
    public Group(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_group, container, false);
    }

    private void RetrieveAndDisplayGroups() {
        userGroup.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterator iterator = dataSnapshot.getChildren().iterator();
                list_of_groups.clear();

                while (iterator.hasNext()) {
                    DataSnapshot dataSnapshot1 = (((DataSnapshot) iterator.next()));
                    list_of_groups.add(dataSnapshot1.getValue(Group_structure.class).name);
                    if(userName == null)
                     userName = (String)(dataSnapshot1).child("members").child("0").getValue();
                }

                list_of_groups.add("Create new");
                arrayAdapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();


        DatabaseReference users = FirebaseDatabase.getInstance().getReference().child("Users");
        userGroup = users.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("groups");
        ListView listView = view.findViewById(R.id.lv_groups);
        arrayAdapter = new ListItemsAdapter(list_of_groups, getContext(), User_group.class, "Create new", listener, userName);
        listView.setAdapter(arrayAdapter );

        RetrieveAndDisplayGroups();
    }

}
