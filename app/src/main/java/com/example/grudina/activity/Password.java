package com.example.grudina.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.grudina.R;
import com.example.grudina.structures.User_structure;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Password extends AppCompatActivity {

    EditText etPassword;
    Button bCreate;
    FirebaseAuth auth;
    FirebaseDatabase db;
    DatabaseReference users;
    FirebaseUser currentUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        initComponents();
    }

    // Edit text && button
    private void initComponents(){
        etPassword = findViewById(R.id.et_login);
        bCreate = findViewById(R.id.b_create);
        etPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPassword.setText("");
            }
        });
        bCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String password = checkPassword();
                if (password != null) {
                    String textLogin = getIntent().getExtras().get("textLogin").toString();
                    addUserAndGoToProfile(textLogin, password);

                } else
                    Toast.makeText(Password.this, "Invalid password", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void addUserAndGoToProfile(final String emailText, final String passwordText) {
        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        db = FirebaseDatabase.getInstance();
        users = db.getReference("Users");

        auth.createUserWithEmailAndPassword(emailText, passwordText)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            String userUid = currentUser.getUid();
                            User_structure user = new User_structure(emailText, passwordText, userUid);
                            users.child(userUid)
                                    .setValue(user);
                            Class destinationActivity = Profile_main.class;
                            Intent profileMainIntent = new Intent(Password.this, destinationActivity);
                            startActivity(profileMainIntent);
                        } else {
                            Toast.makeText(Password.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    //Проверка на пробелы\язык\символы
    private String checkPassword() {
        String outText = etPassword.getText().toString();
        if (outText.length() >= 8) {
            return outText;
        } else return null;
    }
}
