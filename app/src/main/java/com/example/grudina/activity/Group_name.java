package com.example.grudina.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.grudina.User_group_add;
import com.example.grudina.structures.Group_structure;
import com.example.grudina.R;
import com.example.grudina.structures.User_structure;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

public class Group_name extends AppCompatActivity {

    FirebaseAuth auth;
    FirebaseDatabase db;
    DatabaseReference groups;
    DatabaseReference users;
    EditText etName;
    Button bCreateGroupCreating;
//    User_structure currentUserInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__group_name);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        TextView toolbarText = (TextView) toolbar.findViewById(R.id.toolbar2_text);

        setSupportActionBar(toolbar);
        toolbarText.setText("New group");

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Profile_main.class));
            }
        });
        initComponents();
    }


    // Add group to all group and user group
    private void createGroup(final String groupName) {
        auth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance();

        Group_structure group = new Group_structure(groupName);
        Bundle bundle = getIntent().getExtras();
        String userName = getIntent().getExtras().get("userName").toString();
        group.members.add(userName);
        addGroup(group);
        updateUser(group);
    }


    private void addGroup(Group_structure group) {
        groups = db.getReference("Groups");
        groups.child(group.name).setValue(group);
    }

    private void updateUser(final Group_structure group) {
        users = db.getReference("Users");
        DatabaseReference user = users.child(auth.getCurrentUser().getUid());


        user.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

                User_structure p = mutableData.getValue(User_structure.class);

                if (p == null) {
                    Toast.makeText(Group_name.this, "NULL", Toast.LENGTH_LONG).show();
                    return Transaction.success(mutableData);
                }

                p.numberGroupCreated++;
                p.groups.add(group);
                p.history.add("You created group '" + group.name + "'");

                // Set value and report transaction success
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean committed,
                                   DataSnapshot currentData) {
//                Toast.makeText(Group_Creating.this, "", Toast.LENGTH_LONG).show();

            }
        });
    }

    // Edit text && button
    private void initComponents() {
        etName = findViewById(R.id.et_group_name);
        bCreateGroupCreating = findViewById(R.id.b_create_group_creating);

        etName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etName.setText("");
            }
        });

        bCreateGroupCreating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createGroup(etName.getText().toString());
                Intent groupCreatingIntent = new Intent(Group_name.this, User_group.class);
                groupCreatingIntent.putExtra("groupName", etName.getText());
                groupCreatingIntent.putExtra("userName", getIntent().getExtras().get("userName").toString());
                startActivity(groupCreatingIntent);
            }
        });


    }


}
