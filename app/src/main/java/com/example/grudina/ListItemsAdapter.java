package com.example.grudina;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.example.grudina.activity.Group_name;
import com.example.grudina.activity.Module_name;
import com.example.grudina.activity.User_group;
import com.example.grudina.fragments.GroupMember;

import java.util.ArrayList;

public class ListItemsAdapter extends BaseAdapter implements android.widget.ListAdapter {
    private ArrayList<String> list;
    private Context context;
    private Class destinationActivity;
    private String extraButton;
    View.OnClickListener listener;

    public ListItemsAdapter(ArrayList<String> list, Context context, Class destinationActivity) {
        this.list = list;
        this.context = context;
        this.destinationActivity = destinationActivity;
    }

    public ListItemsAdapter(ArrayList<String> list, Context context, Class destinationActivity, String extraButton) {
        this.list = list;
        this.context = context;
        this.destinationActivity = destinationActivity;
        this.extraButton = extraButton;
    }
    public ListItemsAdapter(ArrayList<String> list, Context context, Class destinationActivity, String extraButton, View.OnClickListener listener) {
        this.list = list;
        this.context = context;
        this.destinationActivity = destinationActivity;
        this.extraButton = extraButton;
        this.listener = listener;
    }

    private  String userName;
    public ListItemsAdapter(ArrayList<String> list, Context context, Class destinationActivity, String extraButton,
                            View.OnClickListener listener, String userName) {
        this.list = list;
        this.context = context;
        this.destinationActivity = destinationActivity;
        this.extraButton = extraButton;
        this.listener = listener;
        this.userName = userName;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            view = inflater.inflate(R.layout.custom_row, null);
        }

        //Handle buttons and add onClickListeners
        final Button bGroup = view.findViewById(R.id.b_row_group);


        if (extraButton != null && position == list.size() - 1 && extraButton.equals("Add more")) {
            bGroup.setText(extraButton);
            bGroup.setTextColor(0XF6696969);

            bGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent classIntent = new Intent(context, Module_name.class);
                    context.startActivity(classIntent);
                }
            });
        } else if (extraButton != null && position == list.size() - 1 && extraButton.equals("Create new")) {
            bGroup.setText(extraButton);
            bGroup.setTextColor(0XF6696969);


            bGroup.setOnClickListener(listener);

        } else if (extraButton != null && position ==0 && extraButton.equals("Add people +")) {
            bGroup.setText(extraButton);
            bGroup.setTextColor(0XF6696969);
            bGroup.setOnClickListener(listener);

//            bGroup.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent classIntent = new Intent(context, User_group_add.class);
//                    context.startActivity(classIntent);
//                }
//            });
        }
        else {
            bGroup.setText(list.get((position)));
            bGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(extraButton != null && (extraButton.equals("Create new") || extraButton.equals("Add people +"))){}
                    else {
                        Intent classIntent = new Intent(context, destinationActivity);
                        classIntent.putExtra("groupName", list.get(position));
                        if (userName != null) {
                            classIntent.putExtra("userName", userName);
                        }
                        classIntent.setAction("Profile main");
                        context.startActivity(classIntent);
                    }
                }
            });

        }
        return view;
    }
}