package com.example.grudina;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.grudina.fragments.Group;
import com.example.grudina.fragments.History;
import com.example.grudina.fragments.Inventory;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private Group group;
    private Inventory inventory;
    private History history;

    public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        group = new Group();
        inventory = new Inventory();
        history = new History();
    }

    public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior, View.OnClickListener listener) {
        super(fm, behavior);
        group = new Group(listener);
        inventory = new Inventory();
        history = new History();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return group;
            case 1:
                return inventory;
            case 2:
                return history;
                default:
                    return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        position ++;
        switch (position){
            case 1:
                return "GROUP";
            case  2:
                return "INVENTORY";
            case  3:
                return "ACTIVITY";
                default:
                    return "Error";
        }
    }
}
