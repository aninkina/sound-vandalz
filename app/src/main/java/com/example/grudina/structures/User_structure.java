package com.example.grudina.structures;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.Objects;

public class User_structure {
    public String emailText;
    public String passwordText;
    public Long level;
    public String nickname;
    public String userUid;
    public ArrayList<Group_structure> groups = new ArrayList<>();
    public ArrayList<String> history = new ArrayList<>();
    public ArrayList<Module_structure> modules = new ArrayList<>();

    public Long numberGroupCreated;

    public User_structure(){}

    public User_structure(String emailText, String passwordText, String userUid){
        this.emailText = emailText;
        this.passwordText = passwordText;
        this.userUid = userUid;
        level = 0L;
        numberGroupCreated =0L;
    history.add("profile has registered");
        setNickname();
    }

    public User_structure(String emailText, String passwordText, String userUid,
                          Long level, String nickname, ArrayList<Group_structure> groups,
                          ArrayList<String> history,Long numberGroupCreated,
                          ArrayList<Module_structure> modules){
        this.emailText = emailText;
        this.passwordText = passwordText;
        this.userUid = userUid;
        this.level = level;
        this.nickname = nickname;
        this.groups = groups;
        this.numberGroupCreated = numberGroupCreated;
        this.history = history;
        this.modules = modules;
    }

    private void setNickname(){
        int index = emailText.indexOf('@');
        nickname = emailText.substring(0, index);
    }

    public static User_structure getCurrentUserInfo(@NonNull DataSnapshot dataSnapshot){

        String emailText = (String)dataSnapshot.child("emailText").getValue();
        String passwordText = (String)dataSnapshot.child("passwordText").getValue();
        Long level = (Long)dataSnapshot.child("level").getValue();
        String nickname = (String)dataSnapshot.child("nickname").getValue();
        String userUid = (String)dataSnapshot.child("userUid").getValue();
        Long numberGroups = (Long)dataSnapshot.child("numberGroupCreated").getValue();

       ArrayList<Group_structure> group_data = new ArrayList<>();
       for(DataSnapshot child: dataSnapshot.child("groupsUid").getChildren()){
           group_data.add(new Group_structure(Objects.requireNonNull(child.getValue()).toString()));
       }

        ArrayList<String> history_data = new ArrayList<>();
        for(DataSnapshot child: dataSnapshot.child("history").getChildren()){
            history_data.add((Objects.requireNonNull(child.getValue()).toString()));
        }

        ArrayList<Module_structure> module_data = new ArrayList<>();
        for(DataSnapshot child: dataSnapshot.child("modules").getChildren()){
            module_data.add(new Module_structure(Objects.requireNonNull(child.getValue()).toString()));
        }

        return new User_structure(emailText, passwordText, userUid, level, nickname, group_data, history_data, numberGroups, module_data);
    }

}
