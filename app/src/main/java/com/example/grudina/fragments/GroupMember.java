package com.example.grudina.fragments;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.grudina.ListItemsAdapter;
import com.example.grudina.R;
import com.example.grudina.activity.User_group;
import com.example.grudina.structures.Group_structure;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class GroupMember extends Fragment {
    private ListItemsAdapter arrayAdapter;

    private ArrayList<String> list_of_members = new ArrayList<>();

    private DatabaseReference groupMembers;

    private String groupName;

    private View.OnClickListener listener;

    public GroupMember() {
    }

    public GroupMember(String groupName, View.OnClickListener listener) {
        this.groupName = groupName;
        this.listener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.members_fragment, container, false);
    }

    private void RetrieveAndDisplayGroups() {
        groupMembers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Set<String> set = new HashSet<>();
                Iterator iterator = dataSnapshot.getChildren().iterator();
                while (iterator.hasNext()) {
                    set.add((String)((DataSnapshot) iterator.next()).getValue());
                }

                list_of_members.clear();
                list_of_members.add("Add people +");
                list_of_members.addAll(set);

                arrayAdapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();

        DatabaseReference users = FirebaseDatabase.getInstance().getReference().child("Groups");
//        DatabaseReference groups = users.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("groups");

        groupMembers = users.child(groupName).child("members");
        ListView listView = view.findViewById(R.id.lv_members);

        arrayAdapter = new ListItemsAdapter(list_of_members, getContext(), User_group.class, "Add people +", listener);
        listView.setAdapter(arrayAdapter);

        RetrieveAndDisplayGroups();
    }


}
