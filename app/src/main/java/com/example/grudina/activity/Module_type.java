package com.example.grudina.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.grudina.R;

public class Module_type extends AppCompatActivity {

    Button bModuleTypeChooseRythm;
    Button bModuleTypeChooseBass;
    Button bModuleTypeChooseMelody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_type);

        bModuleTypeChooseRythm = findViewById(R.id.b_module_type_choose_rythm);
        bModuleTypeChooseRythm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent moduleIntent = new Intent(Module_type.this, Map.class);
                moduleIntent.setAction("Choose_type");
                String name = getIntent().getExtras().get("name").toString();
                int type = 0;
                moduleIntent.putExtra("groupName", name);
                moduleIntent.putExtra("type", type);

                startActivity(moduleIntent);
            }
        });
        bModuleTypeChooseBass = findViewById(R.id.b_module_type_choose_bass);
        bModuleTypeChooseBass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent moduleIntent = new Intent(Module_type.this, Map.class);
                moduleIntent.setAction("Choose_type");
                String name = getIntent().getExtras().get("name").toString();
                int type = 1;
                moduleIntent.putExtra("groupName", name);
                moduleIntent.putExtra("type", type);

                startActivity(moduleIntent);
            }
        });
        bModuleTypeChooseMelody = findViewById(R.id.b_module_type_choose_melody);
        bModuleTypeChooseMelody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent moduleIntent = new Intent(Module_type.this, Map.class);
                moduleIntent.setAction("Choose_type");
                String name = getIntent().getExtras().get("name").toString();
                int type = 2;
                moduleIntent.putExtra("groupName", name);
                moduleIntent.putExtra("type", type);

                startActivity(moduleIntent);
            }
        });
    }

    public void crossGoBackClick(View v)
    {
        startActivity(new Intent(getApplicationContext(),Module_name.class));
    }
}
