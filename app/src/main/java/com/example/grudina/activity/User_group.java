package com.example.grudina.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.grudina.R;
import com.example.grudina.User_group_add;
import com.example.grudina.ViewPagerAdapter;
import com.example.grudina.ViewPagerAdapterOne;
import com.example.grudina.structures.User_structure;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class User_group extends AppCompatActivity {
    private android.widget.Toolbar toolbar;
    private ViewPager viewPager;
    private ViewPagerAdapterOne adapter;
    private TabLayout tabLayout;
    private TextView tvNickname;
    private Button bMap;
    FirebaseAuth auth;
    DatabaseReference users;
    private TextView tvLvl;
    User_structure currentUserInfo;
    FirebaseUser currentUser;
    String groupName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_group);
        initComponents0();
        initComponent();
    }


    protected void initComponent() {
        toolbar = findViewById(R.id.toolBar);
        setActionBar(toolbar);
        viewPager = findViewById(R.id.pager2);

        final String userName = getIntent().getExtras().get("userName").toString();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent userGroupIntent = new Intent(User_group.this, User_group_add.class);
                userGroupIntent.putExtra("userName", userName);
                userGroupIntent.putExtra("groupName", groupName);
                startActivity(userGroupIntent);
            }
        };

        adapter = new ViewPagerAdapterOne(getSupportFragmentManager(), 0, groupName, listener);
        viewPager.setAdapter(adapter);
        tabLayout = findViewById(R.id.tabs2);
        tabLayout.setupWithViewPager(viewPager);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        users = FirebaseDatabase.getInstance().getReference();




    }

    private void initComponents0() {
        groupName = getIntent().getExtras().get("groupName").toString();

        TextView tName = findViewById(R.id.toolbar_text);
        tName.setText(groupName);

        Button b = findViewById(R.id.b_create_user_group_creating);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(User_group.this, Profile_main.class);
                Toast.makeText(User_group.this, groupName + " was successfully created", Toast.LENGTH_SHORT).show();
                startActivity(mapIntent);
            }
        });
    }

}