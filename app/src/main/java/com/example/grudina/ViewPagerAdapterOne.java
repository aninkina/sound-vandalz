package com.example.grudina;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.grudina.fragments.Group;
import com.example.grudina.fragments.GroupMember;
import com.example.grudina.fragments.History;
import com.example.grudina.fragments.Inventory;

public class ViewPagerAdapterOne extends FragmentPagerAdapter {
    private GroupMember groupMember;

    public ViewPagerAdapterOne(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }


    public ViewPagerAdapterOne(@NonNull FragmentManager fm, int behavior, String groupName, View.OnClickListener listener) {
        super(fm, behavior);
        groupMember = new GroupMember(groupName, listener);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return groupMember;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        position ++;
        switch (position){
            case 1:
                return "GROUP";
            default:
                return "Error";
        }
    }
}
