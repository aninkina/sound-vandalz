package com.example.grudina.structures;

import java.util.ArrayList;

public class Group_structure {
    public String name;
    public ArrayList<String> members = new ArrayList<>();

    public Group_structure(){}

    public Group_structure(String name)
    {
        this.name = name;
    }
}
