package com.example.grudina.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.grudina.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 */

public class History extends Fragment {

    private View.OnClickListener listener;

    private ListView listView;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> list_of_groups = new ArrayList<>();
    private ArrayList<String> list_reversed = new ArrayList<>();


    private DatabaseReference users;
    private DatabaseReference userHistory;

    public History() {
    }

    public History(View.OnClickListener listener) {
        super();
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    private void RetrieveAndDisplayGroups() {
        userHistory.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Iterator iterator = dataSnapshot.getChildren().iterator();

                list_of_groups.clear();
                while (iterator.hasNext()) {
                    String name =((String)((DataSnapshot) iterator.next()).getValue());
                    list_of_groups.add(list_of_groups.size(), name);
                }
                for(int i = 0; i < list_of_groups.size(); i ++)
                    list_reversed.add(list_of_groups.get(list_of_groups.size() - 1 - i));
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();

        users = FirebaseDatabase.getInstance().getReference().child("Users");
        userHistory = users.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("history");
        listView = view.findViewById(R.id.lv_history);
        arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.custom_row, list_reversed);
        listView.setAdapter(arrayAdapter);


        RetrieveAndDisplayGroups();

    }

}
