package com.example.grudina.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.grudina.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class Sign_in extends AppCompatActivity {

    Button bSignIn;
    EditText etNickname;
    EditText etPassword;
    //авторазация, база данных, работа со списком
    FirebaseAuth auth;
    FirebaseDatabase db;
    DatabaseReference users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        initComponents();
    }

    // if user exists go to "Profile main"
    private void SignInUserAndGoToProfile(String textLogin, String textPassword) {
        auth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance();
        users = db.getReference("Users");

        auth.signInWithEmailAndPassword(textLogin, textPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Class destinationActivity = Profile_main.class;
                    Intent profileMainIntent = new Intent(Sign_in.this, destinationActivity);
                    startActivity(profileMainIntent);
                }
                else {
                    Toast.makeText(Sign_in.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    // Edit text && edit text && button
    private void initComponents(){
        bSignIn = findViewById(R.id.b_sign_in_1);
        etNickname = findViewById(R.id.et_login);
        etPassword = findViewById(R.id.et_password);
        etPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPassword.setText("");
            }
        });
        etNickname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etNickname.setText("");
            }
        });
        bSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textPassword = etPassword.getText().toString();
                String textLogin = etNickname.getText().toString();
                SignInUserAndGoToProfile(textLogin, textPassword);
            }
        });
    }
}
