package com.example.grudina.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.grudina.R;

public class Lock_screen extends AppCompatActivity {

    TextView tvRegister;
    Button bSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lockscreen);
        initComponents();
    }

    // button "sign in" && button "register"
    private void initComponents(){
        // Переход на окно регистрации
        tvRegister = findViewById(R.id.tv_register);
        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Class destinationActivity = Nickname.class;
                Intent profileMainIntent = new Intent(Lock_screen.this, destinationActivity);
                startActivity(profileMainIntent);
            }
        });
        // Переход на окно входа
        bSignIn = findViewById(R.id.b_sign_in);
        bSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Class destinationActivity = Sign_in.class;
                Intent profileMainIntent = new Intent(Lock_screen.this, destinationActivity);
                startActivity(profileMainIntent);
            }
        });
    }
}
