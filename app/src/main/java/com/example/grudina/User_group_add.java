package com.example.grudina;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.grudina.activity.Group_name;
import com.example.grudina.activity.Map;
import com.example.grudina.activity.Profile_main;
import com.example.grudina.activity.User_group;
import com.example.grudina.structures.Group_structure;
import com.example.grudina.structures.User_structure;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class User_group_add extends AppCompatActivity {

    private String groupName;
    private String userName;

    FirebaseAuth auth;
    DatabaseReference users;
    User_structure currentUserInfo;
    FirebaseUser currentUser;
    ArrayAdapter<String> adapter;
    List<String> members = new ArrayList<>();
    ListView lvMain;
    Button addMember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_group_add);

        groupName = getIntent().getExtras().get("groupName").toString();
        userName = getIntent().getExtras().get("userName").toString();



        // находим список
        lvMain = (ListView) findViewById(R.id.lv_add_members);
        lvMain.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lvMain.setItemsCanFocus(false);
        ColorDrawable lvc = new ColorDrawable(this.getResources().getColor(R.color.colorBlack));
        lvMain.setDivider(lvc);
        lvMain.setDividerHeight(1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Group_name.class));
            }
        });

        TextView tv = findViewById(R.id.toolbarT_text);
        tv.setText(groupName);
        // создаем адаптер
        adapter = new ArrayAdapter<String>(this, R.layout.custom_row2, members);;

        // присваиваем адаптер списку
        lvMain.setAdapter(adapter);

        verifyUserExistence();

        Button b = findViewById(R.id.b_user_group_add);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int len = lvMain.getCount();
                SparseBooleanArray checked = lvMain.getCheckedItemPositions();
                for (int i = 0; i < len; i++)
                    if (checked.get(i)) {
                        String item = members.get(i);
                        FirebaseDatabase db = FirebaseDatabase.getInstance();
                        db.getReference().child("Groups").child(groupName).child("members").child(item).setValue(item);

                        Intent mapIntent = new Intent(User_group_add.this, User_group.class);
                        mapIntent.putExtra("groupName", groupName);
                        mapIntent.putExtra("userName", userName);
                        startActivity(mapIntent);
                        /* do whatever you want with the checked item */
                    }
            }
        });



    }

    private void verifyUserExistence(){
        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        users = FirebaseDatabase.getInstance().getReference();


        users.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    Iterator iterator = dataSnapshot.getChildren().iterator();

                    members.clear();
                    while (iterator.hasNext()) {
                        String tempNickname = ((DataSnapshot) iterator.next()).getValue(User_structure.class).nickname;
                        if(!tempNickname.equals(userName))
                        members.add(tempNickname);
                    }

                    adapter.notifyDataSetChanged();

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                 Toast.makeText(User_group_add.this, databaseError.toException().getMessage(), Toast.LENGTH_SHORT).show();


            }
        });
    }
    public void selectUser_Click(View v)
    {
       // addMember=findViewById(R.id.b_row_group2);
        addMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setBackgroundColor(Color.LTGRAY);

            }
        });
    }
}
