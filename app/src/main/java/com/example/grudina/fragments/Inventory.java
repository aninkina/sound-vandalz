package com.example.grudina.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.example.grudina.ListItemsAdapter;
import com.example.grudina.activity.Map;
import com.example.grudina.structures.Module_structure;
import com.example.grudina.R;
import com.example.grudina.activity.Module_name;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Inventory extends Fragment {

    private ListItemsAdapter arrayAdapter;
    private ArrayList<String> list_of_modules = new ArrayList<>();

    private DatabaseReference userModule;

    public Inventory() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inventory, container, false);
    }

    private void RetrieveAndDisplayGroups() {
        userModule.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterator iterator = dataSnapshot.getChildren().iterator();
                list_of_modules.clear();

                while (iterator.hasNext()) {
                    String name =((DataSnapshot) iterator.next()).getValue(Module_structure.class).name;
                    list_of_modules.add(list_of_modules.size(), name);
                }
                list_of_modules.add("Add more");
                arrayAdapter.notifyDataSetChanged();

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();

        DatabaseReference users = FirebaseDatabase.getInstance().getReference().child("Users");
        userModule = users.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("modules");
        ListView listView = view.findViewById(R.id.lv_modules);

        arrayAdapter = new ListItemsAdapter(list_of_modules, getContext(), Map.class, "Add more");
        listView.setAdapter(arrayAdapter);

        RetrieveAndDisplayGroups();
    }

}
