package com.example.grudina.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;


import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.grudina.R;
import com.example.grudina.structures.User_structure;
import com.example.grudina.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Profile_main extends AppCompatActivity {
    private Toolbar toolbar;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private TabLayout tabLayout;
    private TextView tvNickname;
    FirebaseAuth auth;
    DatabaseReference users;
    User_structure currentUserInfo;
    FirebaseUser currentUser;
    private TextView tvLvl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_main);

        initComponent();
        verifyUserExistence();
    }

    private void verifyUserExistence(){
        final String currentUserId = auth.getCurrentUser().getUid();

        users.child("Users").child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child("userUid").exists()){

                    currentUserInfo = User_structure.getCurrentUserInfo(dataSnapshot);
                    Toast.makeText(Profile_main.this, "Welcome " + currentUserInfo.nickname, Toast.LENGTH_SHORT).show();

                    tvNickname = findViewById(R.id.tv_nickname);
                    tvNickname.setText(currentUserInfo.nickname);

                    tvLvl = findViewById(R.id.tv_lvl);
                    tvLvl.setText("LVL " + currentUserInfo.level);
                }
                else{
                    Toast.makeText(Profile_main.this, "You logged first time", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(Profile_main.this, databaseError.toException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void initComponent() {
        toolbar = findViewById(R.id.toolBar);
        setActionBar(toolbar);
        viewPager = findViewById(R.id.pager);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String userName = tvNickname.getText().toString();
                Intent userGroupIntent = new Intent(Profile_main.this, Group_name.class);
                userGroupIntent.putExtra("userName", userName );
                startActivity(userGroupIntent);
            }
        };

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), 0, listener);
        viewPager.setAdapter(adapter);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        users = FirebaseDatabase.getInstance().getReference();

    }





}