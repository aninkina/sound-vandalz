package com.example.grudina.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.grudina.R;
import com.example.grudina.structures.Module_structure;
import com.example.grudina.structures.User_structure;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

public class Module_name extends AppCompatActivity {

    FirebaseAuth auth;
    FirebaseDatabase db;
    DatabaseReference modules;
    DatabaseReference users;
    EditText etName;
    Button bModuleCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_name);
        Toolbar toolbarM = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbarTextM = (TextView) toolbarM.findViewById(R.id.toolbar_text);

        setSupportActionBar(toolbarM);
        toolbarTextM.setText("New module");

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbarM.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new         Intent(getApplicationContext(),Profile_main.class));
            }
        });
        initComponents();
    }


    // Edit text && button
    private void initComponents() {
        etName = findViewById(R.id.et_module_name);
        bModuleCreate = findViewById(R.id.b_create_module_creating);

        etName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etName.setText("");
            }
        });

        bModuleCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createModule(etName.getText().toString());
                Intent moduleIntent = new Intent(Module_name.this, Module_type.class);
                moduleIntent.putExtra("name", etName.getText().toString());
                startActivity(moduleIntent);
            }
        });

    }


    // Add group to all group and user group
    private void createModule(final String groupName) {
        auth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance();

        Module_structure module = new Module_structure(groupName);
        addGroup(module);
        updateUser(module);
    }


    private void addGroup(Module_structure module) {
        modules = db.getReference("Modules");
        modules.child(module.name).setValue(module);
    }

    private void updateUser(final Module_structure module) {
        users = db.getReference("Users");
        DatabaseReference user = users.child(auth.getCurrentUser().getUid());

        user.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

                User_structure p = mutableData.getValue(User_structure.class);

                if (p == null) {
                    Toast.makeText(Module_name.this, "NULL", Toast.LENGTH_LONG).show();
                    return Transaction.success(mutableData);
                }

                p.modules.add(module);
                p.history.add("You created module '" + module.name  +"'");

                // Set value and report transaction success
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean committed,
                                   DataSnapshot currentData) {
                Toast.makeText(Module_name.this, "Nice module name", Toast.LENGTH_LONG).show();

            }
        });
    }



}
