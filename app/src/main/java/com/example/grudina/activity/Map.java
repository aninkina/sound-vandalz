package com.example.grudina.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.grudina.R;
import com.example.grudina.structures.User_structure;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Map extends AppCompatActivity {

    GoogleMap map;
    SupportMapFragment mapFragment;
    FusedLocationProviderClient clientLocation;
    FirebaseDatabase db;

    MarkerOptions markOp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        moduleName = getIntent().getExtras().get("groupName").toString();
        db = FirebaseDatabase.getInstance();
        moduleRef = db.getReference().child("Modules").child(moduleName);

        setMap();
    }


    void setMap() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        clientLocation = LocationServices.getFusedLocationProviderClient(this);

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                LatLng place = new LatLng(57.161297, 65.525017);
                //Move the camera to the user's location and zoom in!
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(place, 18.0f));
                setStyle();


                String action = getIntent().getAction();

                if (action.equals("Profile_main")) {
                    verifyUserExistence();

                    map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            map.addMarker(markOp);

                        }
                    });
                    // nothing
                } else if (action.equals("Choose_type")) {
                    type = (int)getIntent().getExtras().get("type");
                    setAction();
                }
            }
        });
    }

    void setStyle() {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = map.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            Map.this, R.raw.style_json));

            if (!success)
                Toast.makeText(Map.this, "Style parsing failed.", Toast.LENGTH_LONG).show();
        } catch (Resources.NotFoundException e) {
            Toast.makeText(Map.this, "Can't find style error", Toast.LENGTH_LONG).show();
        }
    }

    String moduleName;
    int type;
    DatabaseReference moduleRef;

    void setAction() {
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions()
                        .title(moduleName)
                        .snippet(getImageName(type))
                        .position(latLng).icon(BitmapDescriptorFactory.
                                fromBitmap(setMarkerIcon(getImageName(type), 600, 600)));

                // Animating to the touched position
                map.animateCamera(CameraUpdateFactory.newLatLng(latLng));

//                // Placing a marker on the touched position
                map.addMarker(markerOptions);

                moduleRef.child("x").setValue(latLng.latitude);
                moduleRef.child("y").setValue(latLng.longitude);
                moduleRef.child("type").setValue(type);

                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {

                    }
                });
            }
        });
    }


    private void verifyUserExistence() {

        moduleRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                double x = (double)dataSnapshot.child("x").getValue();
                double y = (double)dataSnapshot.child("y").getValue();
                int type = (int)dataSnapshot.child("type").getValue();
                markOp = new MarkerOptions().title(moduleName)
                        .snippet(getImageName(type))
                        .position(new LatLng(x,y)).icon(BitmapDescriptorFactory.
                                fromBitmap(setMarkerIcon(getImageName(type), 600, 600)));

                // Animating to the touched position
                map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(x,y)));

//                // Placing a marker on the touched position
                map.addMarker(markOp);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(Map.this, databaseError.toException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public Bitmap setMarkerIcon(String iconName, int width, int height) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),
                getResources().getIdentifier(iconName, "drawable", getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }


    String getImageName(int type) {

        switch (type) {
            case 0:
                return "rhythm";
            case 1:
                return "bass";
            case 2:
                return "melody";
            default:
                return null;
        }
    }

    public void profile_Click(View v) {
        startActivity(new Intent(getApplicationContext(), Profile_main.class));
    }

    private void getCurrentLocation() {
        Task<Location> task = clientLocation.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                if (location != null) {
                    mapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
                            MarkerOptions options = new MarkerOptions().position(latlng).title("place");
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 18));
                            googleMap.addMarker(options);
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 44) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCurrentLocation();
            }
        }
    }
}
