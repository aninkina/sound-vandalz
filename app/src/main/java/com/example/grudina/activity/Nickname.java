package com.example.grudina.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.grudina.R;

public class Nickname extends AppCompatActivity {

    EditText etLogin;
    Button bNext;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nickname);
        bNext = findViewById(R.id.b_next);
        etLogin = findViewById(R.id.et_login);
        etLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etLogin.setText("");
            }
        });
        bNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = checkLogin();
                    if (login != null) {
                        Class destinationActivity = Password.class;
                        Intent passwordIntent = new Intent(Nickname.this, destinationActivity);
                        passwordIntent.putExtra("textLogin", etLogin.getText().toString());
                        startActivity(passwordIntent);
                    } else
                        Toast.makeText(Nickname.this, "Invalid login", Toast.LENGTH_LONG).show();
            }
        });
    }

    //Проверка уникальности отсутсвует
    private String checkLogin() {
        String text = etLogin.getText().toString();
        if (!text.equals(""))
            return text;
        else
            return null;
    }
}
